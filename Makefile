#lazy makefile 

CC=g++
EXECUTABLE=zapper
SOURCES=$(wildcard src/*.cpp src/*/*.cpp)
OBJS=$(SOURCES:.cpp=.o)

LIBFLAGS= -lm -ldl  -lz -lSDL_image -ltiff -ljpeg -lpng -lz -lSDL_ttf -lfreetype -lSDL_mixer -lvorbisfile -lvorbis -logg -lstdc++ -lSDL -lGL

#DROIDLIBFLAGS=-lSDL_image -lSDL_net -ltiff -ljpeg -lpng -lz -lSDL_ttf -lfreetype -lSDL_mixer -lsmpeg -lvorbisfile -lvorbis -logg -lstdc++ -Wl --no-undefined -shared -llog

CFLAGS = -Iinclude -I/usr/include/SDL/ -Iinclude/fsm -Iinclude/core -Iinclude/tinyxml -Iinclude/player -Iinclude/level -Iinclude/particles -Iinclude/projectiles -Iinclude/game_states -Iinclude/cups -Iinclude/tips -Iinclude/enemies

debug : $(EXECUTABLE)
release : $(EXECUTABLE)

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	 $(CC) $(OBJS) $(DROIDLIBFLAGS) $(CFLAGS) $(LIBFLAGS)  -o $@
.cpp.o:
	 $(CC) -c $(CFLAGS)  $< -o $@

clean:
	 rm -f   $(EXECUTABLE) $(OBJS)
