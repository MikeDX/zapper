
#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <string>

#define TILE_SIZE 32

#define  BIG_PARTICLE "./data/gfx/particle_big.png"
#define  LITTLE_PARTICLE "./data/gfx/particle.png"

//const char * LITTLE_PARTICLE = "./data/gfx/particle.png";
//const char * BIG_PARTICLE = "./data/gfx/particle_big.png";


#endif /* __CONSTANTS_H__ */
